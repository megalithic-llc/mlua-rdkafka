# mlua-rdkafka Changelog

[0.1.4] - 2024-10-29
### Changed
- [#21](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/21) Upgrade from mlua 0.9.9 → 0.10.0

[0.1.3] - 2024-10-19
### Changed
- [#19](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/19) Upgrade from Rust 1.79.0 → 1.82.0

## [0.1.2] - 2024-07-27
### Changed
- [#18](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/18) Upgrade from Rust 1.75.0 → 1.79.0
- [#17](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/17) Upgrade 5 crates

## [0.1.1] - 2024-01-20
### Added
- [#15](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/15) Support TopicPartitionList struct
- [#12](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/12) Support StreamConsumer with minimal recv() capability
- [#7](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/7) Enable integration tests in the amd64 CI env

### Changed
- [#16](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/16) Upgrade rdkafka from 0.36.1 → 0.36.2
- [#11](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/11) Simplify FutureProducer send() to return (partition, offset) instead of an intermediary UserData
- [#8](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/8) Upgrade Rust from 1.72.1 → 1.75.0

### Fixed
- [#13](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/13) Mutex is not required to mutate OwnedHeaders

## [0.1.0] - 2024-01-14
### Added
- [#6](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/6) Support OwnedDeliveryResult struct
- [#5](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/5) Support OwnedHeaders struct
- [#4](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/4) Support FutureRecord struct
- [#3](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/3) Support FutureProducer struct
- [#2](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/2) Support ClientConfig struct
- [#1](https://gitlab.com/megalithic-llc/mlua-rdkafka/-/issues/1) Put CI+CD in place
