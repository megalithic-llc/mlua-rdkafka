# mlua-rdkafka

Lua bindings for the Rust based [rdkafka](https://crates.io/crates/rdkafka).

[![License](http://img.shields.io/badge/Licence-MIT-blue.svg)](LICENSE)
[![Arch](https://img.shields.io/badge/Arch-aarch64%20|%20amd64-blue.svg)]()
[![Lua](https://img.shields.io/badge/Lua-5.1%20|%205.2%20|%205.3%20|%205.4%20|%20LuaJIT%20|%20LuaJIT%205.2-blue.svg)]()

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-rdkafka --features luajit
```

## Using

```rust
use mlua::Lua;
use mlua_rdkafka;

let lua = Lua::new();
mlua_rdkafka::preload(&lua).unwrap();
let script = r#"
    local rdkafka = require('rdkafka')
    local ClientConfig, OwnedHeaders, FutureRecord = rdkafka.ClientConfig, rdkafka.OwnedHeaders, rdkafka.FutureRecord
    
    -- prepare client
    local client_config = ClientConfig:new()
        :set('group.id', 'mygroup')
        :set('bootstrap.servers', 'mybroker0:9092')
    local future_producer = client_config:create_future_producer()
    
    -- prepare record
    local headers = OwnedHeaders:new():insert('abc', '123')            
    local record = FutureRecord:new():headers(headers)
    
    -- send synchronously
    return future_producer:send(record, 1.0)
"#;
let (partition, offset): (i32, i64) = lua.load(script).eval().unwrap();
```

## Integration Testing

```shell
$ KAFKA_BROKERS=127.0.0.1:9092 KAFKA_TOPIC=abc cargo test --features luajit
```
