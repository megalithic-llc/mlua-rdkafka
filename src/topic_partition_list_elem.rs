use mlua::{AnyUserData, Error, UserData, UserDataMethods};

pub struct TopicPartitionListElem {
    pub topic: String,
    pub partition: i32,
    // TODO offset
    // TODO metadata
    // TODO err
}

impl UserData for TopicPartitionListElem {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_function("eq", |_lua, (this_ud, other_ud): (AnyUserData, AnyUserData)| {
            let this = this_ud
                .borrow::<Self>()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            let other = other_ud
                .borrow::<Self>()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(this.topic == other.topic)
        });
        // TODO err
        // TODO metadata
        // TODO methods.add_method("offset", |_lua, this, ()| Ok(this.delegate.offset()));
        methods.add_method("partition", |_lua, this, ()| Ok(this.partition));
        // TODO set_metadata
        // TODO set_offset
        methods.add_method("topic", |_lua, this, ()| Ok(this.topic.clone()));
    }
}
