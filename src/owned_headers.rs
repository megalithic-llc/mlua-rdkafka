use mlua::{AnyUserData, UserData, UserDataMethods};
use rdkafka::message::Headers;

pub struct OwnedHeaders {
    pub delegate: rdkafka::message::OwnedHeaders,
}

impl UserData for OwnedHeaders {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("count", |_lua, this, ()| Ok(this.delegate.count()));
        methods.add_method("get", |_lua, this, idx: usize| {
            let headers = this.delegate.get(idx - 1);
            let key = headers.key.to_string();
            let value = match headers.value {
                Some(value) => Some(std::str::from_utf8(value)?.to_string()),
                _ => None,
            };
            Ok((key, value))
        });
        methods.add_function(
            "insert",
            |_lua, (ud, key, value): (AnyUserData, String, Option<String>)| {
                let this = ud.borrow::<Self>()?;
                let delegate = {
                    if let Some(value) = value {
                        this.delegate.clone().insert(rdkafka::message::Header {
                            key: &key,
                            value: Some(&value),
                        })
                    } else {
                        let value: Option<&str> = None;
                        this.delegate
                            .clone()
                            .insert(rdkafka::message::Header { key: &key, value })
                    }
                };
                Ok(OwnedHeaders { delegate })
            },
        );
        methods.add_method("new", |_lua, this, ()| {
            Ok(OwnedHeaders {
                delegate: this.delegate.clone(),
            })
        });
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn count() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local OwnedHeaders = require('rdkafka').OwnedHeaders
            local owned_headers = OwnedHeaders:new()
            owned_headers = owned_headers:insert('abc', '123')
            owned_headers = owned_headers:insert('def', '234')
            return owned_headers:count() 
        "#;
        let count: usize = lua.load(script).eval()?;
        assert_eq!(count, 2);
        Ok(())
    }

    #[test]
    fn get() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local OwnedHeaders = require('rdkafka').OwnedHeaders
            local owned_headers = OwnedHeaders:new():insert('abc', '123')
            return owned_headers:get(1) 
        "#;
        let (k, v): (String, String) = lua.load(script).eval()?;
        assert_eq!(k, "abc");
        assert_eq!(v, "123");
        Ok(())
    }

    #[test]
    fn new() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local OwnedHeaders = require('rdkafka').OwnedHeaders
            local owned_headers = OwnedHeaders:new()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn new_and_insert() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local OwnedHeaders = require('rdkafka').OwnedHeaders
            local owned_headers = OwnedHeaders:new():insert('abc', '123')
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
