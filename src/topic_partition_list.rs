use crate::topic_partition_list_elem::TopicPartitionListElem;
use mlua::{AnyUserData, Error, UserData, UserDataMethods};

pub struct TopicPartitionList {
    pub delegate: rdkafka::TopicPartitionList,
}

impl UserData for TopicPartitionList {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_function(
            "add_partition",
            |_lua, (ud, topic, partition): (AnyUserData, String, i32)| {
                let mut this = ud
                    .borrow_mut::<Self>()
                    .map_err(|err| Error::RuntimeError(err.to_string()))?;
                let topic_partition_list_elem = this.delegate.add_partition(topic.as_str(), partition);
                Ok(TopicPartitionListElem {
                    topic: topic_partition_list_elem.topic().to_string(),
                    partition: topic_partition_list_elem.partition(),
                })
            },
        );
        methods.add_function(
            "add_partition_range",
            |_lua, (ud, topic, start_partition, stop_partition): (AnyUserData, String, i32, i32)| {
                let mut this = ud
                    .borrow_mut::<Self>()
                    .map_err(|err| Error::RuntimeError(err.to_string()))?;
                this.delegate
                    .add_partition_range(topic.as_str(), start_partition, stop_partition);
                Ok(())
            },
        );
        methods.add_method("capacity", |_lua, this, ()| Ok(this.delegate.capacity()));
        methods.add_method("count", |_lua, this, ()| Ok(this.delegate.count()));
        methods.add_method("elements", |_lua, this, ()| {
            let elements = this.delegate.elements();
            Ok(elements
                .iter()
                .map(|x| TopicPartitionListElem {
                    topic: x.topic().to_string(),
                    partition: x.partition(),
                })
                .collect::<Vec<TopicPartitionListElem>>())
        });
        methods.add_function("elements_for_topic", |_lua, (this_ud, topic): (AnyUserData, String)| {
            let this = this_ud
                .borrow::<Self>()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            let elements = this.delegate.elements_for_topic(topic.as_str());
            Ok(elements
                .iter()
                .map(|x| TopicPartitionListElem {
                    topic: x.topic().to_string(),
                    partition: x.partition(),
                })
                .collect::<Vec<TopicPartitionListElem>>())
        });
        methods.add_function(
            "find_partition",
            |_lua, (ud, topic, partition): (AnyUserData, String, i32)| {
                let this = ud
                    .borrow::<Self>()
                    .map_err(|err| Error::RuntimeError(err.to_string()))?;
                let topic_partition_list_elem = this.delegate.find_partition(topic.as_str(), partition);
                Ok(topic_partition_list_elem.map(|x| TopicPartitionListElem {
                    topic: x.topic().to_string(),
                    partition: x.partition(),
                }))
            },
        );
    }
}
