use mlua::Lua;
use std::env;
use std::error::Error;
extern crate tokio;

const KAFKA_BROKERS: &str = "KAFKA_BROKERS";
const KAFKA_TOPIC: &str = "KAFKA_TOPIC";

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn produce() -> Result<(), Box<dyn Error>> {
    let brokers = match env::var(KAFKA_BROKERS) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", KAFKA_BROKERS);
            return Ok(());
        }
    };
    let topic = match env::var(KAFKA_TOPIC) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", KAFKA_TOPIC);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local rdkafka = require('rdkafka')
        local ClientConfig, FutureRecord = rdkafka.ClientConfig, rdkafka.FutureRecord
        
        local client_config = ClientConfig:new()
            :set('bootstrap.servers', '_brokers_')
        local producer = client_config:create_future_producer()
        
        local record = FutureRecord:new():topic('_topic_'):key('key'):payload('value')
        return producer:send(record, 1.0)
    "#
    .replace("_brokers_", &brokers)
    .replace("_topic_", &topic);
    let (partition, offset): (i32, i64) = lua.load(script).eval_async().await?;
    assert!(partition > -1);
    assert!(offset > -1);
    Ok(())
}

#[tokio::test(flavor = "multi_thread", worker_threads = 1)]
async fn consume() -> Result<(), Box<dyn Error>> {
    let brokers = match env::var(KAFKA_BROKERS) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", KAFKA_BROKERS);
            return Ok(());
        }
    };
    let topic = match env::var(KAFKA_TOPIC) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", KAFKA_TOPIC);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local rdkafka = require('rdkafka')
        local ClientConfig = rdkafka.ClientConfig
        
        local client_config = ClientConfig:new()
            :set('bootstrap.servers', '_brokers_')
            :set('auto.offset.reset', 'earliest')
            :set('group.id', 'mlua-rdkafka/src/integration_tests.rs')
        local consumer = client_config:create_stream_consumer()
        consumer:subscribe('_topic_')
        local message = consumer:recv()
        return message:timestamp(), message:topic(), message:partition(), message:offset(), message:key(), message:payload()
    "#
    .replace("_brokers_", &brokers)
    .replace("_topic_", &topic);
    let (timestamp, topic, partition, offset, key, payload): (f64, String, i32, i64, bstr::BString, bstr::BString) =
        lua.load(script).eval_async().await?;
    eprintln!(
        "got: timestamp={} topic={} partition={} offset={} key={:?} payload={:?}",
        timestamp, topic, partition, offset, key, payload
    );
    Ok(())
}
