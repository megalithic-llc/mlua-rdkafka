use crate::owned_headers::OwnedHeaders;
use bstr::BString;
use mlua::{UserData, UserDataMethods};
use rdkafka::{Message, Timestamp};

pub struct OwnedMessage {
    pub(crate) delegate: rdkafka::message::OwnedMessage,
}

impl UserData for OwnedMessage {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_meta_method("__print", |_lua, this, ()| Ok(format!("{:?}", this.delegate)));
        methods.add_method("headers", |_lua, this, ()| {
            let headers = this.delegate.headers().map(|headers| OwnedHeaders {
                delegate: headers.clone(),
            });
            Ok(headers)
        });
        methods.add_method("key", |_lua, this, ()| {
            let key = this.delegate.key().map(BString::from);
            Ok(key)
        });
        methods.add_method("offset", |_lua, this, ()| {
            let offset = this.delegate.offset();
            Ok(offset)
        });
        methods.add_method("partition", |_lua, this, ()| {
            let partition = this.delegate.partition();
            Ok(partition)
        });
        methods.add_method("payload", |_lua, this, ()| {
            let payload = this.delegate.payload().map(BString::from);
            Ok(payload)
        });
        methods.add_method("timestamp", |_lua, this, ()| {
            let timestamp = match this.delegate.timestamp() {
                Timestamp::CreateTime(timestamp) => Some(timestamp as f64 / 1e3),
                _ => None,
            };
            Ok(timestamp)
        });
        methods.add_method("topic", |_lua, this, ()| {
            let topic = this.delegate.topic().to_string();
            Ok(topic)
        });
    }
}
