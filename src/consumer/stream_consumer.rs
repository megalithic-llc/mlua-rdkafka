use crate::owned_message::OwnedMessage;
use mlua::{AnyUserData, Error, UserData, UserDataMethods, Variadic};
use rdkafka::consumer::Consumer;

pub(crate) struct StreamConsumer {
    pub delegate: rdkafka::consumer::StreamConsumer,
}

impl UserData for StreamConsumer {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("assignment_lost", |_lua, this, ()| Ok(this.delegate.assignment_lost()));
        methods.add_async_method("recv", |_lua, this, ()| async move {
            let message = this
                .delegate
                .recv()
                .await
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            let owned_message = OwnedMessage {
                delegate: message.detach(),
            };
            Ok(owned_message)
        });
        methods.add_function(
            "store_offset",
            |_lua, (ud, topic, partition, offset): (AnyUserData, String, i32, i64)| {
                let this = ud.borrow::<Self>()?;
                this.delegate
                    .store_offset(topic.as_str(), partition, offset)
                    .map_err(|err| Error::RuntimeError(err.to_string()))?;
                Ok(())
            },
        );
        methods.add_function("subscribe", |_lua, (ud, topics): (AnyUserData, Variadic<String>)| {
            let topics: Vec<&str> = topics.iter().map(|topic| topic.as_str()).collect();
            let this = ud.borrow::<Self>()?;
            this.delegate
                .subscribe(topics.as_slice())
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_function("unassign", |_lua, ud: AnyUserData| {
            let this = ud.borrow::<Self>()?;
            this.delegate
                .unassign()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_function("unsubscribe", |_lua, ud: AnyUserData| {
            let this = ud.borrow::<Self>()?;
            this.delegate.unsubscribe();
            Ok(())
        });
    }
}
