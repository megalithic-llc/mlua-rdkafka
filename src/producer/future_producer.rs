use super::FutureRecord;
use mlua::{Error, FromLua, MultiValue, UserData, UserDataMethods};
use std::time::Duration;

pub struct FutureProducer {
    pub delegate: rdkafka::producer::FutureProducer,
}

impl UserData for FutureProducer {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_async_method("send", |lua, this, args: MultiValue| async move {
            // Get args
            let future_record_ud: FutureRecord = args[0].as_userdata().unwrap().take()?;
            let duration_f64 = f64::from_lua(args[1].clone(), &lua)?;

            // Parse future record UserData arg into an rdkafka FutureRecord
            let mut future_record: rdkafka::producer::FutureRecord<[u8], [u8]> =
                rdkafka::producer::FutureRecord::to(&future_record_ud.topic);
            if let Some(ref headers) = future_record_ud.headers {
                future_record.headers = Some(headers.clone());
            }
            if let Some(ref key) = future_record_ud.key {
                future_record.key = Some(key.as_slice());
            }
            if let Some(partition) = future_record_ud.partition {
                future_record.partition = Some(partition);
            }
            if let Some(ref payload) = future_record_ud.payload {
                future_record.payload = Some(payload.as_slice());
            }
            if let Some(timestamp) = future_record_ud.timestamp {
                future_record.timestamp = Some(timestamp);
            }

            // Parse duration float arg into an rdkafka Duration
            let duration = Duration::from_secs_f64(duration_f64);

            // Send
            let (partition, offset) = this
                .delegate
                .send(future_record, duration)
                .await
                .map_err(|(err, _)| Error::RuntimeError(err.to_string()))?;
            Ok((partition, offset))
        });
    }
}
