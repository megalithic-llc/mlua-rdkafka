use crate::owned_headers::OwnedHeaders;
use mlua::{UserData, UserDataMethods, Value};

#[derive(Clone, Default)]
pub struct FutureRecord {
    pub topic: String,
    pub partition: Option<i32>,
    pub payload: Option<Vec<u8>>,
    pub key: Option<Vec<u8>>,
    pub timestamp: Option<i64>,
    pub headers: Option<rdkafka::message::OwnedHeaders>,
}

impl UserData for FutureRecord {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("headers", |_lua, this, arg: Value| {
            let owned_headers: OwnedHeaders = arg.as_userdata().unwrap().take()?;
            let mut future_record = this.clone();
            future_record.headers = Some(owned_headers.delegate.clone());
            Ok(future_record)
        });
        methods.add_method("key", |_lua, this, key: bstr::BString| {
            let mut future_record = this.clone();
            future_record.key = Some(key.to_vec());
            Ok(future_record)
        });
        methods.add_method("new", |_lua, _this, ()| Ok(FutureRecord::default()));
        methods.add_method("partition", |_lua, this, partition: i32| {
            let mut future_record = this.clone();
            future_record.partition = Some(partition);
            Ok(future_record)
        });
        methods.add_method("payload", |_lua, this, payload: bstr::BString| {
            let mut future_record = this.clone();
            future_record.payload = Some(payload.to_vec());
            Ok(future_record)
        });
        methods.add_method("timestamp", |_lua, this, timestamp: i64| {
            let mut future_record = this.clone();
            future_record.timestamp = Some(timestamp);
            Ok(future_record)
        });
        methods.add_method("topic", |_lua, this, topic: String| {
            let mut future_record = this.clone();
            future_record.topic = topic;
            Ok(future_record)
        });
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn headers() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local rdkafka = require('rdkafka')
            local OwnedHeaders, FutureRecord = rdkafka.OwnedHeaders, rdkafka.FutureRecord

            local headers = OwnedHeaders:new():insert('abc', '123')            
            local future_record = FutureRecord:new():headers(headers)
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn new() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local FutureRecord = require('rdkafka').FutureRecord
            local future_record = FutureRecord:new()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
