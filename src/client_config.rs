use crate::consumer::StreamConsumer;
use crate::producer::FutureProducer;
use mlua::{Error, FromLua, MultiValue, UserData, UserDataMethods};

pub struct ClientConfig {
    pub delegate: rdkafka::ClientConfig,
}

impl UserData for ClientConfig {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("create_future_producer", |_lua, this, ()| {
            let client_config: &rdkafka::ClientConfig = &this.delegate;
            let delegate: rdkafka::producer::FutureProducer = client_config
                .create()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(FutureProducer { delegate })
        });
        methods.add_method("create_stream_consumer", |_lua, this, ()| {
            let client_config: &rdkafka::ClientConfig = &this.delegate;
            let delegate: rdkafka::consumer::StreamConsumer = client_config
                .create()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(StreamConsumer { delegate })
        });
        methods.add_method("new", |_lua, this, ()| {
            let delegate = this.delegate.clone();
            Ok(ClientConfig { delegate })
        });
        methods.add_method("set", |lua, this, args: MultiValue| {
            let k = String::from_lua(args[0].clone(), lua)?;
            let v = String::from_lua(args[1].clone(), lua)?;
            let mut delegate = this.delegate.clone();
            delegate.set(k, v);
            Ok(ClientConfig { delegate })
        });
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn create_future_producer() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local rdkafka = require('rdkafka')
            local ClientConfig = rdkafka.ClientConfig
            local future_producer = ClientConfig:new():create_future_producer()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn new() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local rdkafka = require('rdkafka')
            local ClientConfig = rdkafka.ClientConfig
            local client_config = ClientConfig:new()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn new_and_set() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local rdkafka = require('rdkafka')
            local ClientConfig = rdkafka.ClientConfig
            
            local client_config = ClientConfig:new()
                :set('group.id', 'abc')
                :set('bootstrap.servers', 'def')
                :set('enable.partition.eof', 'false')
                :set('session.timeout.ms', '6000')
                :set('enable.auto.commit', 'true')
                :set('auto.commit.interval.ms', '5000')
                :set('enable.auto.offset.store', 'false')
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
