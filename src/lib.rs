mod client_config;
mod consumer;
mod owned_headers;
mod owned_message;
mod producer;
mod topic_partition_list;
mod topic_partition_list_elem;

use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let table = lua.create_table()?;
    table.raw_set(
        "ClientConfig",
        lua.create_userdata(client_config::ClientConfig {
            delegate: rdkafka::ClientConfig::new(),
        })?,
    )?;
    table.raw_set("FutureRecord", lua.create_userdata(producer::FutureRecord::default())?)?;
    table.raw_set(
        "OwnedHeaders",
        lua.create_userdata(owned_headers::OwnedHeaders {
            delegate: rdkafka::message::OwnedHeaders::new(),
        })?,
    )?;

    // Preload module
    let globals = lua.globals();
    let package: Table = globals.get("package")?;
    let loaded: Table = package.get("loaded")?;
    loaded.set("rdkafka", table)?;
    Ok(())
}

#[cfg(test)]
mod integration_tests;

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let module: Table = lua.load("return require('rdkafka')").eval()?;
        assert!(module.contains_key("ClientConfig")?);
        assert!(module.contains_key("OwnedHeaders")?);
        Ok(())
    }
}
